package pruebas.appprueba.Entidades;

public class Conocimientos {

    private int id;
    private String Conocimientos;

    // Nuevamente creamos el constructor de la clase
    public Conocimientos(int id, String conocimiento){
        this.id  = id;
        this.Conocimientos = conocimiento;
    }

    public String getConocimientos() {
        return Conocimientos;
    }

    public void setConocimientos(String conocimientos) {
        Conocimientos = conocimientos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}

